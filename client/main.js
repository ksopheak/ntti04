import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import '../route/router.js';
import './main.html';
import './AppLayout/AppLayout.html';
import './TabBar/TabBar.html';
import './HomePage/HomePage.html';
import './Sport/Sport.html';

import './AboutUs/AboutUs.html';