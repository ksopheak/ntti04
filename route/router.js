import '../client/AppLayout/AppLayout.html'; 
Router.configure({ 
    layoutTemplate : 'AppLayout' 
  }); 
Router.route('/', function () { 
    this.render('HomePage'); 
}); 
Router.route('/Sport', function () { 
    this.render('Sport'); 
});
Router.route('/AboutUs', function () { 
    this.render('AboutUs'); 
});